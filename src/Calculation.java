import java.util.HashMap;

public class Calculation {

    public static void main(String[] args) {

        EconomCar econCar1 = new EconomCar("Economic", "Daewoo Lanos",
                140, 11, 8000);
        EconomCar econCar2 = new EconomCar("Economic", "Dacia Logan",
                110, 9, 6000);
        StandardCar standCar1 = new StandardCar("Standard", "Mazda CX5",
                190, 8, 30000);
        StandardCar standCar2 = new StandardCar("Standard", "Skoda Octavia",
                175,10, 28000);
        PremiumCar premCar1 = new PremiumCar("Premium", "BMW 750",
                245,15, 50000);
        PremiumCar premCar2 = new PremiumCar("Premium", "Toyota Camry",
                230,13, 48000);
        PremiumCar premCar3 = new PremiumCar("Premium", "Tesla Model S",
                280,0, 70000);

        /* Fleet cost calculation */
        int[] priceCars = new int[7];
        priceCars[0] = econCar1.sumPriceCar();
        priceCars[1] = econCar2.sumPriceCar();
        priceCars[2] = standCar1.sumPriceCar();
        priceCars[3] = standCar2.sumPriceCar();
        priceCars[4] = premCar1.sumPriceCar();
        priceCars[5] = premCar2.sumPriceCar();
        priceCars[6] = premCar3.sumPriceCar();
        int sumPriceCars = 0;

        for (int priceCar : priceCars) {
            sumPriceCars += priceCar;
        }
        System.out.println("Fleet cost: " + sumPriceCars);
        System.out.println(" ");

        //* Sort fleet vehicles by fuel consumption */
        HashMap<Integer, String> map = new HashMap<>();
        map.put(econCar1.getConsumptionFuel(), econCar1.getNameCar());
        map.put(econCar2.getConsumptionFuel(), econCar2.getNameCar());
        map.put(standCar1.getConsumptionFuel(), standCar1.getNameCar());
        map.put(standCar2.getConsumptionFuel(), standCar2.getNameCar());
        map.put(premCar1.getConsumptionFuel(), premCar1.getNameCar());
        map.put(premCar2.getConsumptionFuel(), premCar2.getNameCar());
        map.put(premCar3.getConsumptionFuel(), premCar3.getNameCar());

        map.keySet().stream().sorted().forEach(key -> {
                String value = map.get(key);
                System.out.println(value + " - fuel consumption: " + key);
            }
        );
        System.out.println(" ");

        //* Find a car according to the specified conditions */
        // The cheapest car in each category
        System.out.println("The typeCar is economic");
        econCar1.CheepCar(econCar1.getNameCar(), econCar1.getPriceCar(),
                econCar2.getNameCar(), econCar2.getPriceCar());
        System.out.println(" ");

        System.out.println("The typeCar is standard");
        standCar1.CheepCar(standCar1.getNameCar(), standCar1.getPriceCar(),
                standCar2.getNameCar(), standCar2.getPriceCar());
        System.out.println(" ");

        System.out.println("The typeCar is premium");
        premCar1.CheeperCar(premCar1.getNameCar(), premCar1.getPriceCar(),
                premCar2.getNameCar(), premCar2.getPriceCar(),
                premCar3.getNameCar(), premCar3.getPriceCar());
        System.out.println(" ");

        // The fastest car in the Premium category
        premCar1.MaxSpeedCar(premCar1.getNameCar(), premCar1.getMaxSpeed(),
                premCar2.getNameCar(), premCar2.getMaxSpeed(),
                premCar3.getNameCar(), premCar3.getMaxSpeed());
    }
}
