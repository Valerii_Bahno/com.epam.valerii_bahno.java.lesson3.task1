class StandardCar extends Car implements FindCheepCar {

    public StandardCar(String typeCar, String nameCar, int maxSpeed,
                       int consumptionFuel, int priceCar) {
        super(typeCar, nameCar, maxSpeed, consumptionFuel, priceCar);
    }

    @Override
    int sumPriceCar() {
        return getPriceCar();
    }

    @Override
    public void CheepCar(String name1, int price1, String name2, int price2) {
        int min = Math.min(price1, price2);
        if (min == price1) {
            System.out.println("The cheeper car is " + name1 + " - " + min);
        }
        else {
            System.out.println("The cheeper car is " + name2 + " - " + min);
        }
    }
}