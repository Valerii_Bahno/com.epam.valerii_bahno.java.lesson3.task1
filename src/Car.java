abstract class Car {

    private String typeCar;
    private String nameCar;
    private int maxSpeed;
    private int consumptionFuel;
    private int priceCar;

    public Car(String typeCar, String nameCar, int maxSpeed, int consumptionFuel, int priceCar) {
        this.typeCar = typeCar;
        this.nameCar = nameCar;
        this.maxSpeed = maxSpeed;
        this.consumptionFuel = consumptionFuel;
        this.priceCar = priceCar;
    }

    public String getTypeCar() {
        return typeCar;
    }

    public String getNameCar() {
        return nameCar;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getConsumptionFuel() {
        return consumptionFuel;
    }

    public int getPriceCar() {
        return priceCar;
    }

    abstract int sumPriceCar();
}
